#pragma once

#include <WinSock2.h>
#include <Windows.h>	
#include "Helper.h"		
#include <fstream>		
#include <set>			
#include <queue>		
#include <utility>      
#include <string>       
#include <thread>		
#include <mutex>		
#include <iostream>		
						
#define CLEAN5 "00000"	
#define CLEAN2 "00"		
						
class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:

	void accept();
	void clientHandler(SOCKET clientSocket);
	std::string get_login_Message(SOCKET clientSocket);
	void send_login_Message(SOCKET clientSocket);
	void clientUpdateMessage(SOCKET clientSocket, std::string client_name);
	void disconnect(SOCKET clientSocket, std::string client_name);


	void run_files();

	//helper
	std::string get_all_users();
	std::string make_file_to_string(std::string file_path);

	
	std::queue< std::pair<std::string, std::string> > _allMsgs;
	std::set<std::string> _allUsers;
	SOCKET _serverSocket;
};