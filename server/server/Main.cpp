#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>
#include <string>  


int give_port();

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;
		myServer.serve(give_port());
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}


/*
will open config file and will return the port that in the file
*/
int give_port()
{
	std::string port_line = "";
	std::ifstream config_file;
	config_file.open("config.txt", std::ios::app);

	getline(config_file, port_line);//the server_ip -> do not need this now
	getline(config_file, port_line);//will take the port line
	config_file.close();
	std::string port = "";
	for (int i = 0; i < port_line.size(); i++)
	{
		if (i >= 5)
		{
			port += port_line[i];
		}
	}
	return stoi(port);
}