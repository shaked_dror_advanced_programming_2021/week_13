#include "Server.h"
#include <exception>
#include <iostream>
#include <string>

std::mutex mtx1;//for the file 
std::condition_variable cond;

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	//will start the run of the files msgs
	std::thread file_update(&Server::run_files, this);
	file_update.detach();


	while (true)
	{
		//the main thread is only accepting clients 
		//and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	//this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	//the function that handle the conversation with the client
	std::thread client_chat(&Server::clientHandler, this, client_socket);
	client_chat.detach();
}


void Server::clientHandler(SOCKET clientSocket)
{
	std::string client_name = "";
	try
	{
		/*the login*/
		//the first msg from the client is a login msgs
		client_name = get_login_Message(clientSocket);	
		send_login_Message(clientSocket);

		while (true)
		{
			int msg_type = Helper::getMessageTypeCode(clientSocket);
			//if the msg type is 204 need to get the client msg
			if (msg_type == MT_CLIENT_UPDATE)
			{
				clientUpdateMessage(clientSocket, client_name);
			}
			if (msg_type == MT_CLIENT_EXIT)
			{
				this->disconnect(clientSocket, client_name);
				break;
			}
		}

		//Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket);
	}
	catch (const std::exception& e)
	{
		this->disconnect(clientSocket, client_name);
		closesocket(clientSocket);
	}
}

/*
the function will take the login msg from the client and will take the client
*/
std::string Server::get_login_Message(SOCKET clientSocket)
{
	int size = this->_allUsers.size();
	//the msg must be a login msg, 200 = login msg
	if (Helper::getMessageTypeCode(clientSocket) != MT_CLIENT_LOG_IN)
	{
		throw;//will close the socket and disconnect the client
	}
	else
	{	
		int name_size = Helper::getIntPartFromSocket(clientSocket, 2);//will gate the name size
		std::string client_name = Helper::getStringPartFromSocket(clientSocket, name_size);
		this->_allUsers.insert(client_name);//will put the name in a set
		//if the user is already exists will not let the user to login and will kick him out 
		if (size == this->_allUsers.size())
		{
			throw;
		}
		return client_name;
	}
}

/*
will send to the client a login msg in the server format
*/
void Server::send_login_Message(SOCKET clientSocket)
{
	Helper::send_update_message_to_client(clientSocket, "", "", this->get_all_users());
}

/*
will take the client msg and will put it in the queue
*/
void Server::clientUpdateMessage(SOCKET clientSocket, std::string client_name)
{
	int size = Helper::getIntPartFromSocket(clientSocket, 2);//the len size of the second user 
	std::string chat_content = "";
	std::string second_username = "";

	if (size != 0)//if size is 0 no one is supposed to receive the message
	{
		second_username = Helper::getStringPartFromSocket(clientSocket, size);//the second user name

		//will make the file name by the abc value
		std::string combine = "";
		if (client_name < second_username)
		{
			combine = client_name + "&" + second_username;
		}
		else
		{
			combine = second_username + "&" + client_name;
		}
		
		//will get the msg from file
		chat_content = this->make_file_to_string(combine);

		size = Helper::getIntPartFromSocket(clientSocket, 5);//the len size of the msg
		if (size != 0)//if size is 0 there is no msg to send
		{
			std::string msg = Helper::getStringPartFromSocket(clientSocket, size);//the msg

			chat_content += "&MAGSH_MESSAGE&&Author&" + client_name + "&DATA&" + msg;//the texts postsand the current texts

			//will put the msg in a queue for the file		
			std::pair<std::string, std::string> temp = { combine , "&MAGSH_MESSAGE&&Author&" + client_name + "&DATA&" + msg };
			//so that the file does not read the information at the time of writing
			std::unique_lock<std::mutex> locker(mtx1);
			this->_allMsgs.push(temp);
			locker.unlock();
			cond.notify_all();//will tell the file that he can start writing
		}
	}

	//will send the chat to the client
	Helper::send_update_message_to_client(clientSocket, chat_content, second_username, get_all_users());
}

/*
will disconnect client
*/
void Server::disconnect(SOCKET clientSocket, std::string client_name)
{
	std::cout << client_name << " disconnected " << std::endl;
	this->_allUsers.erase(client_name);
	closesocket(clientSocket);
}

/*
will run on the queue of msgs and will put them on a files by the path
*/
void Server::run_files()
{
	while (true)
	{
		std::unique_lock<std::mutex> locker(mtx1);
		cond.wait(locker, [&]() {return !_allMsgs.empty();});//will wait until there's a msg in the list
		while (!this->_allMsgs.empty()) //will run on all the msgs and will put them in a file that suits them
		{
			std::pair<std::string, std::string> temp = this->_allMsgs.front();//will save the msg and the file path without the .txt
			std::ofstream users_file;
			users_file.open(temp.first + ".txt", std::ios::app);//temp.first is the file path
			//will put the msg in the file
			users_file << temp.second;//temp.second is the msg data
			users_file.close();
			this->_allMsgs.pop();
		}
		locker.unlock();
		cond.notify_all();//will tell the file that he can start writing
	}
}

/*
will give all the users as string with & 
*/
std::string Server::get_all_users()
{
	//will make the user string with &
	std::string allUsers_string = "";
	std::set<std::string>::iterator i;
	for (i = this->_allUsers.begin(); i != this->_allUsers.end(); i++)
	{
		allUsers_string += *i + "&";
	}

	//will delete the last & from the string of users
	if (!allUsers_string.empty())
	{
		allUsers_string.pop_back();
	}

	return allUsers_string;
}

/*
will open a file and will return the file as string
*/
std::string Server::make_file_to_string(std::string file_path)
{
	std::string line;
	std::string all= "";
	std::ifstream my_file(file_path + ".txt");
	if (my_file.is_open())//if the file exist
	{
		while (getline(my_file, line))
		{
			all += line;//adding the file line to the msg
		}
		my_file.close();
		return all;
	}
	else
	{
		my_file.close();
		return "";
	}
}
